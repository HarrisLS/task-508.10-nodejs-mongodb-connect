const express = require("express");

const router = express.Router();

router.get("/courses", (req, res) => {
  res.status(200).json({
    message: "GET all Course",
  });
});

router.get("/courses/:courseId", (req, res) => {
  let courseId = req.params.courseId;
  res.status(200).json({
    message: "GET Course Id = " + courseId,
  });
});

router.post("/courses", (req, res) => {
  res.status(200).json({
    message: "Greate Course",
  });
});

router.put("/courses/:courseId", (req, res) => {
  let courseId = req.params.courseId;
  res.status(200).json({
    message: "Update Course Id = " + courseId,
  });
});

router.delete("/courses/:courseId", (req, res) => {
  let courseId = req.params.courseId;
  res.status(200).json({
    message: "Delete Course Id = " + courseId,
  });
});

module.exports = router;
