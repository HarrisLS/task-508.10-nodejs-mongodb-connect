const express = require("express");

const router = express.Router();

router.get("/reviews", (req, res) => {
  res.status(200).json({
    message: "GET all review",
  });
});
router.get("/reviews/:reviewId", (req, res) => {
  let reviewId = req.params.reviewId;
  res.status(200).json({
    message: "GET review Id = " + reviewId,
  });
});
router.post("/reviews", (req, res) => {
  res.status(200).json({
    message: "Create review",
  });
});
router.put("/reviews/:reviewId", (req, res) => {
  let reviewId = req.params.reviewId;
  res.status(200).json({
    message: "Update review Id = " + reviewId,
  });
});
router.delete("/reviews/:reviewId", (req, res) => {
  let reviewId = req.params.reviewId;
  res.status(200).json({
    message: "Delete review Id = " + reviewId,
  });
});
module.exports = router;
