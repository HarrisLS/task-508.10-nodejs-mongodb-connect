// Import thư viện mongoose
const mongoose = require("mongoose");

// Khai báo class Schema của thư viện mongooseJs
const Schema = mongoose.Schema;

// Khai báo course schema
const courseSchema = new Schema(
  {
    title: {
      type: String,
      require: true,
      unique: true,
    },
    description: {
      type: String,
      require: false,
    },
    noStudent: {
      type: Number,
      default: 0,
    },
    review: [
      {
        type: mongoose.Types.ObjectId,
        ref: "reviews",
      },
    ],
  },
  {
    timestamps: true,
  }
);
module.exports = mongoose.model("courses", courseSchema);
